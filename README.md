## Installation

You can install **active-campaign-python** by downloading the source.

[Click here to download the source (.zip)](https://gitlab.com/bart.bania/activecampaign-api-python/repository/archive.zip) which includes all dependencies.

`from includes.ActiveCampaign import ActiveCampaign`

Fill in your URL and API Key in the `includes/Config.py` file, and you are good to go!
